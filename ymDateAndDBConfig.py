# from datetime import timedelta, date
import datetime
import MySQLdb

# def daterange(start_date, end_date):

#     for n in range(int((end_date - start_date).days)+1):
#         yield start_date + timedelta(n)

# def mergeAllFileWithDateColumnAdd():
#     today = datetime.datetime.now()
#     start_date = today + datetime.timedelta(-8)
#     end_date = today + datetime.timedelta(-2)
#     for single_date in daterange(start_date, end_date):
#         cuurentDate = single_date.strftime("%Y-%m-%d")
#         if single_date.month == datetime.datetime.now().month:
#             print "Same Month - " + cuurentDate
#         else:
#             print "Not a Same Month - " + cuurentDate


def getFirstStartEndDates():
    startEndDateConfig = {}
    # start_date = datetime.datetime.strptime("20180830",'%Y%m%d')
    # end_date = datetime.datetime.strptime("20180907",'%Y%m%d')
    today = datetime.datetime.now()
    start_date = today + datetime.timedelta(-8)
    end_date = today + datetime.timedelta(-2)
    startEndDateConfig['firstDay'] = start_date.replace(day=1)
    startEndDateConfig['startDate'] = start_date
    startEndDateConfig['endDate'] = end_date
    startEndDateConfig['today'] = today
    return startEndDateConfig


def getDB():
    myCnfFile = open("my.cnf", "w")
    host = "localhost"
    port = 3306
    user = "root"
    password = "yukta-staging"
    #password = "root"
    database = "dev_frankly_360"

    myCnfFileContains = "[client]\nhost="+host+"\n\nuser="+user+"\npassword='"+password+"'\nloose-local-infile=1"   
    myCnfFile.write(myCnfFileContains + '\n')    
    db = MySQLdb.connect(host=host, port=port, user=user, passwd=password, db=database, autocommit=True)
    return db

# getFirstStartEndDates()
