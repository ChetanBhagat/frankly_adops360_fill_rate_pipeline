import csv
import sys
import re


def readFromCSVFile(fileName):
    try:
        csvReader = csv.DictReader(open(fileName, 'r'))
        return csvReader
    except Exception as err:
        print(err)
        exit(0)


def setSourceFiles(folderName, fileName, sourceName, tagType, salesType):
    inputOutPutFileFolder = folderName + "/"
    fileName = inputOutPutFileFolder + fileName
    fileData = readFromCSVFile(fileName)
    outPutFile = inputOutPutFileFolder + folderName + "_AddedColumns.csv"
    getUniqueValues(outPutFile, fileData, sourceName, tagType, salesType)


#  "ADX" "ADX_PrepareMerged.csv" "Google AdExchange" "NonHB"  "Programmatic"
def getUniqueValues(outPutFile, sourceData, sourceName, tagType, salesType):
    mergeOutPutDataList = []
    isHeaderRequired = True
    for row in sourceData:
        row['Source_Provider_rpt'] = sourceName
        #row['Tag_Type_rpt'] = tagType
        #row['Ad_Type_rpt'] = ''
        #row['Sales_Type_rpt'] = salesType
        #row['Inventory_rpt'] = ''
        row['Device_Type1_rpt'] = ''
        #row['Device_Type2_rpt'] = ''
        # row['Placement_rpt'] = '' 
        row['Creative_Size_rpt'] = ''
        #row['Rate_Card_Size_rpt'] = ''
        row['Station_rpt'] = ''
        row['DFP_Ad_unit2'] = ''
        # inventory = ''

	if 'newsweek' in row['Ad unit 1']:
            row['Station_rpt'] = 'newsweek'
        else:
            row['Station_rpt'] = row['Ad unit 2']
        if len(["Device category"]):
            if "Desktop" in row["Device category"]:
                row['Device_Type1_rpt'] = "Desktop"
            else:
                row['Device_Type1_rpt'] = "Mobile"
        else:
            row['Device_Type1_rpt'] = "Mobile"

        if len(row['Requested ad sizes']):
            creativeSizeExtract = re.split(",", row['Requested ad sizes'])
            # print(creativeSizeExtract[0])
            row['Creative_Size_rpt'] = creativeSizeExtract[0]
            
 
        mergeOutPutDataList.append(row)
    writeToCSVFile(mergeOutPutDataList, outPutFile, isHeaderRequired)


def writeToCSVFile(dictList, fileName, isHeaderRequired):
    if len(dictList):
        with open(fileName, 'w') as csvFile:
            dictWriter = csv.DictWriter(csvFile, dictList[0].keys())
            if isHeaderRequired:
                dictWriter.writeheader()
            dictWriter.writerows(dictList)
            print ("Data downloaded into ===> " + fileName)
    else:
        print ("There is no data for download for the CSV ===> " + fileName)


#setSourceFiles("house_ads", "House_Ads.csv", "House_Ads", " ", " ")
#setSourceFiles("Supply", "Supply_raw.csv", "Supply", "Non_HB", "Programatic")

setSourceFiles(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
print (" ----- Add Extra Coloumns Run Successfully ----- ")
