import csv
import sys
import datetime
import pandas as pd
import numpy as np


def readFromCSVFile(fileName):
    try:
        csvReader = csv.DictReader(open(fileName, 'r'))
        return csvReader
    except Exception as err:
        print("\033[91m {}\033[00m" .format(err))
        exit(0)


def setSourceFiles(sourceName, sourceDataFile, sourceMappingFile, columnsForMapping, revenueColumnName, dateColumnName):
    print("\n* " + "\033[96m {}\033[00m" .format(sourceName) + " * : ")
    inputOutPutFileFolder = sourceName + "/"
    listOfColumnsForMapping = columnsForMapping.split(',')
    sourceMappingFile = inputOutPutFileFolder +  sourceMappingFile    
    sourceMappingData = readFromCSVFile(sourceMappingFile)
    placementSizeUnique = getUniqueValues(sourceMappingData, listOfColumnsForMapping)
    # print placementSizeUnique
    sourceData = readFromCSVFile(inputOutPutFileFolder + sourceDataFile)
    mergeOutPutFileName = inputOutPutFileFolder + sourceName + "_PrepareMerged.csv"
    nonMergeOutPutDataAndMappingKeys = doMappingAndMerge(sourceData, placementSizeUnique, mergeOutPutFileName, listOfColumnsForMapping, dateColumnName)
    # -- getting non merge data
    unknownMappValuesList = nonMergeOutPutDataAndMappingKeys[2]
    nonMergeData = nonMergeOutPutDataAndMappingKeys[1]
    currentDateTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
    unknownMappingFileName = "Z_UnMapped_Files/" + sourceName + "_Unknown_Mappings_" + currentDateTime + ".csv"
    # -- getting distinct columns's value of mapping columns
    # unknownMappValuesListToCsvFile(unknownMappValuesList, unknownMappingFileName, listOfColumnsForMapping)
    # -- getting distinct columns's value  of mapping columns and sum of revenue
    #getCsvOfSumOfRevenueOfUnknownTags(unknownMappValuesList, unknownMappingFileName, nonMergeData, listOfColumnsForMapping, revenueColumnName, dateColumnName)
    print("\033[92m {}\033[00m" .format("Successfully Run Prepare Mapping for --- " + sourceName))


def writeToCSVFile(dictList, fileName, isHeaderRequired):
    if len(dictList):
        with open(fileName, 'w') as csvFile:        
            dictWriter = csv.DictWriter(csvFile, dictList[0].keys())
            if isHeaderRequired:
                dictWriter.writeheader()
            dictWriter.writerows(dictList)
            if "_Unknown_Mappings_" in fileName:
                print("\033[95m {}\033[00m" .format("Data downloaded into ===> " + fileName))
            else:
                print("\033[92m {}\033[00m" .format("Data downloaded into ===> " + fileName))

    # else:
    #     print "There is no data for download for the CSV ===> " + fileName


def getUniqueValues(sourceData, listOfColumnsForMapping):
    placementSizeUnique = {}
    for row in sourceData:
        keyValues = []
        for x in listOfColumnsForMapping:
            keyValues.append(row[x])
        mappingColumnValuesKey = "###".join(str(x).strip().lower() for x in keyValues)
        placementSizeUnique[mappingColumnValuesKey] = row
    return placementSizeUnique


def doMappingAndMerge(sourceData, placementSizeUnique, mergeOutPutFileName, listOfColumnsForMapping, dateColumnName):
    mergeOutPutDataList = []
    nonMergeOutPutDataList = []
    nonMergeOutPutDataAndMappingKeys = []
    unknownMappValuesList = set()
    unknownMappValuesListWithDate = set()
    isHeaderRequired = True
    listOfColumnsForMappingWithDate = []
    listOfColumnsForMappingWithDate = list(listOfColumnsForMapping)
    listOfColumnsForMappingWithDate.append(dateColumnName)
    for row in sourceData:
        keyValues = []
        keyValuesWithDate = []
        for x in listOfColumnsForMapping:
            keyValues.append(row[x])
        for xWithDate in listOfColumnsForMappingWithDate:
            keyValuesWithDate.append(row[xWithDate])
        # mappingColumnValuesKey = "###".join(str(x).split()[0] for x in keyValues)
        mappingColumnValuesKey = "###".join(str(x).strip().lower() for x in keyValues)
        mappingColumnValuesKeyWithDate = "###".join(str(x).strip().lower() for x in keyValuesWithDate)
        if mappingColumnValuesKey in placementSizeUnique:
            row.update(placementSizeUnique[mappingColumnValuesKey])
            mergeOutPutDataList.append(row)
        else:
            unknownMappValuesList.add(mappingColumnValuesKey)
            nonMergeOutPutDataList.append(row)
            unknownMappValuesListWithDate.add(mappingColumnValuesKeyWithDate)
    mergeOutPutDataList.extend(nonMergeOutPutDataList)
    writeToCSVFile(mergeOutPutDataList, mergeOutPutFileName, isHeaderRequired)
    nonMergeOutPutDataAndMappingKeys.append(unknownMappValuesList)
    nonMergeOutPutDataAndMappingKeys.append(nonMergeOutPutDataList)
    nonMergeOutPutDataAndMappingKeys.append(unknownMappValuesListWithDate)
    return nonMergeOutPutDataAndMappingKeys


def unknownMappValuesListToCsvFile(unknownMappValuesList, unknownMappingFileName, listOfColumnsForMapping):
    unknownDataList = []
    isHeaderRequired = True
    for row in unknownMappValuesList:
        unknownMappKeyValuesList = {}
        i = 0       
        for x in listOfColumnsForMapping:
            unknownMappKeyValuesList[x] = row.split("###")[i]
            i = i + 1
        unknownDataList.append(unknownMappKeyValuesList)
    writeToCSVFile(unknownDataList, unknownMappingFileName, isHeaderRequired)

def getCsvOfSumOfRevenueOfUnknownTags(unknownMappValuesList, unknownMappingFileName, nonMergeData, listOfColumnsForMapping, revenueColumnName, dateColumnName):
    listOfColumnsForMapping.append(dateColumnName)    
    isHeaderRequired = True
    outPutAll = [] 
    for a in unknownMappValuesList:
        # outPutSingle = {}
        sum1 = 0.0
        unknownMappKeyValuesList = {}
        i = 0 
        for row in nonMergeData:
            keyValues = []
            for x in listOfColumnsForMapping:
                keyValues.append(row[x])
            mappingColumnValuesKey = "###".join(str(x).strip().lower() for x in keyValues)
            if a == mappingColumnValuesKey:
                sum1 = sum1 + float(row[revenueColumnName].replace("$", ""))
        for x in listOfColumnsForMapping:
            unknownMappKeyValuesList[x] = a.split("###")[i]
            i = i + 1
        # outPutSingle['Sites'] = a
        # outPutSingle['sum_revenue'] = sum1
        unknownMappKeyValuesList[revenueColumnName] = sum1
        outPutAll.append(unknownMappKeyValuesList)
    writeToCSVFile(outPutAll, unknownMappingFileName, isHeaderRequired)


# setSourceFiles("Supply", "Supply_raw.csv","Supply_site_mapping_station.csv", "App names", " ", "Date")
setSourceFiles(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])
