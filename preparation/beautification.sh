#! /bin/bash


# Supply prepare=================================
printf "\n \e[0;36m * Supply File * \e[0m \n"
cd ./Supply
sh Supply_unwanted.sh
cd ..

# Hosue_Ads prepare=================================
printf "\n \e[0;36m * House_Ads File * \e[0m \n"
cd ./House_Ads
python house_beautification.py
printf "\n \e[0;36m * House_Ads File beautified and save as House_Ads.csv * \e[0m \n"
