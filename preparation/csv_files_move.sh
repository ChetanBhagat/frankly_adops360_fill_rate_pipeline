#! /bin/bash

cd -
cd CSVs/
DATE=`date +%Y-%m-%d`
cd -
python CSVs/CSV_file_check.py

cd ./Supply
[[ -f Supply_raw_$DATE.csv ]] && mv Supply_raw_$DATE.csv Supply_raw.csv

cd ..
cd ./House_Ads
[[ -f House_Ads_raw_$DATE.csv ]] && mv House_Ads_raw_$DATE.csv House_Ads_raw.csv
