﻿import pandas as pd 
# import numpy as np


house_ads_df = pd.read_csv('House_Ads_raw.csv')
house_ads_df['source'] = 'House_Ads'
house_ads_df = house_ads_df[:-1]
house_ads_df['time_key'] = pd.to_datetime(pd.Series(house_ads_df['Date']),format="%m/%d/%y").dt.strftime("%Y%m%d")
house_ads_df.to_csv('House_Ads.csv',index=False)
# print (house_ads_df)
