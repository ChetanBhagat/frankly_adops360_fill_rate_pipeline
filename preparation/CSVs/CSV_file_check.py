import glob, os
from shutil import copyfile
import datetime
import mail_v1
import shutil
import errno

now = datetime.datetime.now()
print now.strftime("%Y-%m-%d")
cur_date = now.strftime("%Y-%m-%d") 

def getFilesNames(): 
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    files = []
    for file in glob.glob("*"+cur_date+".csv"):
        #print(file)
        files.append(file)

#    for file in glob.glob("*"+cur_date+".xls"):
    for file in glob.glob("*.xlsx"):
        files.append(file)
	print file
    
    try:
        os.makedirs('OLD_CSV/'+cur_date)
    except OSError as e:
        if e.errno != errno.EEXIST:
            # raise
            pass
    
    return files

def fileMove(SourceName,Filelist,Pathfolder):
    for f in Filelist:
        if "DFP" in f or "Passback" in f:
            shutil.copy2(''+f+'', '../DFPData')
            shutil.copy2(''+f+'','OLD_CSV/'+cur_date)
            os.remove(f)
        else:
            shutil.copy2(''+f+'', '../'+Pathfolder+'')
            shutil.copy2(''+f+'','OLD_CSV/'+cur_date)
            os.remove(f)
        # print("Files copied succuessfully for=========>",f)

def filesCheck(ifcondition, numberoffiles, sourceName,folderpath, files):
    fileNames = []
    csvnames = ''
    mailmessage = ''

    for row in files:
        if eval(ifcondition):
            fileNames.append(row) 
            csvnames += row+ ","
    if len(fileNames) == numberoffiles:
        print("\033[92m {}\033[00m" .format("All files present for =====>"+sourceName))    
        fileMove(sourceName,fileNames,folderpath)
    else:        
        print("\033[91m {}\033[00m" .format("Files not present for == "+sourceName+ " File Names ==> "+csvnames)) 
        mailmessage += sourceName+ "Files not present=====>"+csvnames+'\n'
    # print(mailmessage)
    return mailmessage


files = getFilesNames()
FinalMail = ''
# print(files)


#Supply_raw
Supplyifcondition = '"Supply_raw" in row '
FinalMail += filesCheck(Supplyifcondition,1,'Supply','Supply',files)

#House_Ads
houseadsifcondition = '"House_Ads_raw" in row '
FinalMail += filesCheck(houseadsifcondition,1,'House_Ads','House_Ads',files)


print(FinalMail)
mail_v1.send_mail("house_unfilled CSV Files Not Find", FinalMail)
