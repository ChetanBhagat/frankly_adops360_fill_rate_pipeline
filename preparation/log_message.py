import datetime

now = datetime.datetime.now()
print now.strftime("%Y-%m-%d")
cur_date = now.strftime("%Y-%m-%d") 

def logMessage(message):
    filename = 'LogFile_'+cur_date+'.txt'    
    with open('../LOG/'+filename, 'a') as csvFile:
        csvFile.write(message+'\n')

# date,Source name, Step Name, Task Name, Start tiime, End time,  Status, Error Message
# logMessage("Hello")