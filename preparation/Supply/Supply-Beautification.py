import csv
import os
from datetime import datetime
import time
import sys
sys.path.insert(0, '../../')
import log_message


SOURCE_FILE_NAME = 'Supply_raw.csv'
FINAL_FILE_NAME = 'output_Supply.csv'

def csvOpenxProcess(sourcefilename):
    csvread = ''
    read = csv.reader(open(sourcefilename,'r'))
    for row in read:
        # print(row)
        if not row:
            pass
        elif row[0] == ' ':
            pass   
        elif row[0] == '':
            pass   
        elif row[0] == 'Total':
            pass   
        else:
            # print(row)
            for line in range(0,len(row)):
                # print (row[line])
                csvread += '"' + row[line] + '",'
            csvread = csvread[:-1]
            csvread += '\n'
            
    # print(csvread)
    return csvread

def writeDownload(filename, datawrite):
    with open(filename, 'w') as csvFile:
        csvFile.write(datawrite)

def addColumns(FINAL_FILE_NAME):
    with open(FINAL_FILE_NAME) as csvfile:
        reader = csv.DictReader(csvfile)
        header_string = ''
        rows_string = ''
        output_string = ''
        for row in reader:
            for key,value in row.items():
                header_string += '"' + key + '",'
            header_string = header_string[:-1]
            header_string += "\n"
            # print(header_string)
            break

    with open(FINAL_FILE_NAME) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            for key, value in row.items():
                if key == 'Date':
                    rows_string += '"' + datetime.strptime(value, "%m/%d/%y").strftime("%Y%m%d") + '",'
                else:
                    rows_string += '"' + value + '",'                
            rows_string = rows_string[:-1]            
            rows_string += '\n'
    output_string += header_string + rows_string
    writeDownload(FINAL_FILE_NAME, output_string)




# date,Source name, Step Name, Task Name, Start time, End time,  Status, Error Message
msg = ''
try:
    date = (str(datetime.now().strftime("%y-%m-%d")))
    source = 'Supply'
    step_name = 'Supply Beautification'
    task_name = 'Beautification'
    start_time = str(datetime.now())
    dataread = csvOpenxProcess(SOURCE_FILE_NAME)
    writeDownload(FINAL_FILE_NAME,dataread)
    addColumns(FINAL_FILE_NAME)
    print("Successfull")
    end_time = str(datetime.now())
    status = 'Success'
    error_message = ''
except Exception as err:
    print (Exception, err)
    end_time = str(datetime.now())
    status = 'Failed'
    error_message = str(err)
msg += date+","+source+","+step_name+","+task_name+","+start_time+","+end_time+","+status+","+error_message
print(msg)
log_message.logMessage(msg)
