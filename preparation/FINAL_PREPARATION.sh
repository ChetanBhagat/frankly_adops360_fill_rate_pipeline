#!/bin/bash


printf "\n\n \e[1;37m***** PREPARATION AUTOMATION STARTED for Supply and house *****\e[0m"


printf "\n\n \e[1;33m*** CSV file check and Move started ***\e[0m \n\n"
./csv_files_move.sh
printf "\n \e[1;33m*** CSV file check and Move Completed ***\e[0m \n"


printf "\n\n \e[1;33m*** Beautification started ***\e[0m \n\n"
./beautification.sh
printf "\n \e[1;33m*** Beautification Completed ***\e[0m \n"


printf "\n\n \e[1;33m*** Adding derived column started ***\e[0m \n\n"
./run_mapping_n_add_columns.sh
printf "\n \e[1;33m*** Adding derived column Completed ***\e[0m \n"


printf "\n\n \e[1;33m*** CSV file move to Merge started ***\e[0m \n\n"
./csv_file_move_to_preprocessing.sh
printf "\n \e[1;33m*** CSV file move to Merge Completed ***\e[0m \n"

printf "\n\n \e[1;33m*** CSV file move to Prepared files to Prepared_CSVs_Old folder started ***\e[0m \n\n"
./moving_prepared_files.sh
printf "\n\n \e[1;33m*** CSV file move to Prepared files to Prepared_CSVs_Old folder Completed ***\e[0m \n\n"

printf "\n\n \e[1;37m***** PREPARATION AUTOMATION COMPLETED *****\e[0m \n"
