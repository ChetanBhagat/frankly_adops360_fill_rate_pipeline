import datetime
import os
now = datetime.datetime.now()
# print now.strftime("%Y-%m-%d")
cur_date = now.strftime("%Y-%m-%d") 

def logMessage(message,processname='prepare'):
    #filename = 'LogFile_'+cur_date+'.txt' 
    filename = 'FranklyProgramticLog.txt'
    if processname == 'merge' or processname == 'preprocess':
        path = '../LOG/'
    elif processname == 'prepare':
        path = '../../LOG/'
    if os.stat(path+filename).st_size == 0:
        with open(path+filename, 'w') as csvFile:
            csvFile.write("date,source,step_name,task_name,start_time,end_time,status,error_message\n")
            csvFile.write(message+'\n')
    else:
        with open(path+filename, 'a') as csvFile:
			csvFile.write(message+'\n')

# date,Source name, Step Name, Task Name, Start tiime, End time,  Status, Error Message
# logMessage("Hello")
