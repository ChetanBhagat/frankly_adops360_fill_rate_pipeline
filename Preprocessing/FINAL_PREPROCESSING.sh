printf "\n\n \e[1;33m*** CSV file loading to database started ***\e[0m \n\n"

printf "\n Supply data loading \n"
mysql --defaults-file=my.cnf "dev_frankly_360" < "sql/Supply.sql"

printf "\n House_Ads data loading \n"
mysql --defaults-file=my.cnf "dev_frankly_360" < "sql/House_Ads.sql"

printf "\n\n \e[1;33m*** CSV file loading to database complete ***\e[0m \n\n"

printf "\n\nCSV file move to data started\n\n"
./moving_csv_files_to_data.sh
printf "\n\nCSV file move to data complete\n\n"
