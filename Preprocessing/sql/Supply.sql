truncate table daily_supply_house_p_temp;

LOAD DATA LOCAL INFILE 'output_Supply.csv' 
INTO TABLE daily_supply_house_p_temp 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' IGNORE 1 ROWS 
(@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10, 
@col11, @col12, @col13, @col14, @col15, @col16, @col17, @col18, @col19, @col20, @col21,
@col22, @col23, @col24, @col25,@col26,@col27)

set time_key = replace(STR_TO_DATE(substring_index(@col14, '/', 3), '%m/%d/%y'), '-', ''),
region = @col8,
station = @col9,
sub_station_name =@col6,
source =@col21, 
adUnit_4 = @col3,
adUnit_5 = @col4, 
derived_device_type1_name = @col20, 
derived_tag_type_name = @col18,
creative_size_rpt_name = @col16,
demand_channel = @col11,
total_code_served_count= replace(@col10,",",""),
unfilled_impressions= replace(@col19,",","");

delete from daily_supply_house_p
where source = 'Supply' and time_key between (select min(time_key) from daily_supply_house_p_temp) and (select max(time_key) from daily_supply_house_p_temp);

insert into daily_supply_house_p select * from daily_supply_house_p_temp;
