truncate table daily_supply_house_p_temp;

LOAD DATA LOCAL INFILE 'output_House_Ads.csv'   INTO TABLE daily_supply_house_p_temp FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS (@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10,     @col11, @col12, @col13, @col14, @col15, @col16, @col17, @col18, @col19, @col20, @col21, @col22) 
set time_key = @col14, 
region = @col6,
 station = @col7, 
 sub_station_name = @col5,
 source = @col17,
 adUnit_4 = @col2, 
 adUnit_5 = @col3,
 derived_device_type1_name = @col22,
 creative_size_rpt_name = @col13,
 house_ads_impressions = replace(@col15,",","");


delete from daily_supply_house_p
where source = 'House_Ads' and time_key between (select min(time_key) from daily_supply_house_p_temp) and (select max(time_key) from daily_supply_house_p_temp);


insert into daily_supply_house_p select * from daily_supply_house_p_temp;

